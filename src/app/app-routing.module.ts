import { BookViewComponent } from './components/book-view/book-view.component';
import { ListItemComponent } from './components/list-item/list-item.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';

const routes: Routes = [{ path: 'home', component: HomeComponent},
{ path: 'test', component: ListItemComponent},
{ path: 'pannel', component: BookViewComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
