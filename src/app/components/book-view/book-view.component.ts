import { Router } from '@angular/router';
import { Component } from '@angular/core';
import { BookSVCService } from 'src/app/services/book-svc.service';

@Component({
  selector: 'app-book-view',
  templateUrl: './book-view.component.html',
  styleUrls: ['./book-view.component.scss']
})
export class BookViewComponent{


  private urlBook ="https://www.gutenberg.org/cache/epub/84/pg84-images.html"
  public titleBook="Mocked Book"
  private routeState: any;
//  public bookContent:string=""//"BookBookBookBookBookBookBook BookBookBookBookBookBookBookBookBook BookBookBookBookBookBookBookBookBookBook BookBookBookBookBookBookBookBookBook BookBookBookBookBook"

  constructor(private router: Router, private bookSvc:BookSVCService) {
    if (this.router.getCurrentNavigation()?.extras.state) {
      this.routeState = this.router.getCurrentNavigation()?.extras.state;
      if (this.routeState) {
        this.urlBook = this.routeState.bookUrl ? this.routeState.bookUrl : '';
        this.titleBook = this.routeState.titleBook ? this.routeState.titleBook : '';
      }

    }
this.getBook();
}


  getBook(){
    var book = this.bookSvc.downloadBook(this.urlBook).subscribe((res)=>{
      //this.bookContent = res;
      this.formatDoc(res);
    },(err)=>{
      console.error(err)
    })
    
    
  }
  formatDoc(bookEl:string){
    
    
    var doc = document.implementation.createHTMLDocument("");
    doc.open();
    doc.write(bookEl);
    doc.close();

    //UpdateHref
    var pgInter:HTMLCollectionOf<Element>= doc.getElementsByClassName("pginternal");
    for(var i=0; i<pgInter.length; i++){
        let initVal =pgInter.item(i)?.getAttribute('href')
        pgInter.item(i)?.setAttribute("href", "/pannel"+initVal)
      }
      document.getElementById('bookContent')?.appendChild(doc.body);
      
    }
}
