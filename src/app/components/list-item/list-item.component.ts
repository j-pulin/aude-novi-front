import { NONE_TYPE } from '@angular/compiler';
import { Component,Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss']
})
export class ListItemComponent  {

  @Input() public author:string= ''//"J.R Tolkien";
  
  @Input() public title:string=''//"La communauté de l'anneau";

  @Input() public coverUrl:string=''//"https://static.fnac-static.com/multimedia/Images/FR/NR/40/67/1e/1992512/1507-1/tsp20171002094855/La-communaute-de-l-anneau.jpg";
  
  @Input() public bookUrl:string=''//"https://www.gutenberg.org/cache/epub/84/pg84-images.html";
  
  @Input() public id:Number=Number();

  constructor(private router:Router) { }

  viewBook() {

    this.router.navigate(['/pannel'], {
      state: {
        "bookUrl":this.bookUrl,
        "titleBook":this.title
    }});
  
  }
}
