import { Component, OnInit } from '@angular/core';
import { BookSVCService } from 'src/app/services/book-svc.service';

@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {


  public showAdvanced:boolean=false;
  constructor(private bookSvc:BookSVCService) { }

  public books:any[] = []

  ngOnInit(): void {
  }

  inputSearchChange(event:any, type:string){
    var val = event.target.value
    this.books=[]
    this.bookSvc.findGeneric(type,val).subscribe(
      (res:any)=>{
        if(res.length!=0){
        this.books = res
      }else{
        alert("Aucun livre contenant \""+val+"\" indexé :c")
      }

        console.log(res)
      },
      (err)=>{
        console.log(err)
      })
  }

  regexChange(event:any){
    this.books=[]
    var val = event.target.value
    this.bookSvc.getBookByRegex(val).subscribe(
      (res:any)=>{

        res.map((d:any)=>{  
          this.getBookById(d)
        })
      },
      (err)=>{
        console.log(err)
      })

  }

  getBookById(bookId:any){

    this.bookSvc.findGeneric("bookId",bookId).subscribe((res)=>{
      this.books.push(res)
    },(err)=>{
      console.error(err)
    })
    
  }

}
