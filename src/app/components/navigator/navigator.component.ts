import { Component, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';

@Component({
  selector: 'app-navigator',
  templateUrl: './navigator.component.html',
  styleUrls: ['./navigator.component.css']
})
export class NavigatorComponent {

  @ViewChild('sidenav') sidenav?:MatSidenav;
  isExpanded = false;
  showSubmenu = false;
  isShowing = false;
  showSubSubMenu = false;
  router = '';
  constructor(){}

  // Chaque entré fait référence à une url (path), une icon (material.icon) et le nom à affiché dans la barre de navigation
  // registred path -> app-routing.module

  infos = [
    {path: 'home', icon: 'home', name: 'Recherche'},
    {path: 'pannel'  , icon: 'visibility_on', name: 'Voir livre'},
    {path: 'test', icon: 'build', name: 'test'},
    {path: 'login', icon: 'logout', name: 'Logout'}
  ];

  mouseenter(): void {
    if (!this.isExpanded) {
      this.isShowing = true;
    }
  }
  mouseleave(): void {
    if (!this.isExpanded) {
      this.isShowing = false;
    }
  }


}
