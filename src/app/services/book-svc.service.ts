
import { HttpClient, HttpClientModule, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { map} from 'rxjs';
@Injectable({
  providedIn: 'root'
})

export class BookSVCService {

  constructor(private httpClient:HttpClient) { }




  public findGeneric(field:string, value:string){

    var headers = new HttpHeaders()
    .set("Accept", "*/*")
    return this.httpClient.get(environment.apiURL+"/genericBook/"+field+"/"+value, {headers});
  }

  public getBook(val:any){
    var headers = new HttpHeaders()
    .set("Accept", "*/*")
    var body = {"title":val};
    return this.httpClient.post(environment.apiURL+'/findByTitle', body, {headers}) 
  }
  
  public getBookByRegex(val:any){

    var headers = new HttpHeaders()
    .set("Accept", "*/*")
    var body = {"regex":val};
    return this.httpClient.post(environment.apiURL+'/findByRegex', body, {headers}) 
  }

  public downloadBook(url:string){

    var headers = new HttpHeaders()
//    .set('Content-Type', 'application/json')
    .set("Accept", "*/*")

    var body={
      url
    }
    return this.httpClient.post(environment.apiURL+'/downloadBook',body, {headers,  responseType: 'text' as const})
  }

}
