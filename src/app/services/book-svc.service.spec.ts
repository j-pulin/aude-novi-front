import { TestBed } from '@angular/core/testing';

import { BookSVCService } from './book-svc.service';

describe('BookSVCService', () => {
  let service: BookSVCService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BookSVCService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
