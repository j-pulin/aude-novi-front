
#Etape 1, installation des dépendances npm et packaging de l'application 
FROM node:14.16-alpine as install 
WORKDIR /tmp
#Install dependences
COPY ./package.json /tmp
RUN npm install
COPY . /tmp

#package app
RUN npm run-script build

# Step 2 installation du package sur un serveur web nginx
FROM nginx:1.17.9-alpine
WORKDIR /usr/share/nginx/html

#Suppression de la conf / defaut de nginx
RUN rm /usr/share/nginx/html/index.html
RUN rm /etc/nginx/conf.d/default.conf

#Add package to nginx working dir
COPY --from=install /tmp/dist/aude-novi /usr/share/nginx/html

#Add nginx conf
COPY nginx.conf /etc/nginx/conf.d/nginx.conf

#Expose container on port 80 (HTTP)
EXPOSE 80

#Start nginx when container is up
CMD ["nginx", "-g", "daemon off;"]
